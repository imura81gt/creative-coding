import p5 from 'p5';
const FRAME_RATE = 60;
const MAX_RECT_SIZE = 100;
const MIN_RECT_SIZE = 35;
const sketch = (p: p5) => {
  p.setup = () => {
    const canvas = p.createCanvas(200, 200);
    p.frameRate(FRAME_RATE);
    canvas.parent('app3');
    p.pixelDensity(1);
  };
  p.draw = () => {
    let x = Math.random() * p.width;
    let y = Math.random() * p.height;
    let w = Math.random() * (MAX_RECT_SIZE - MIN_RECT_SIZE) + MIN_RECT_SIZE;
    let h = Math.random() * (MAX_RECT_SIZE - MIN_RECT_SIZE) + MIN_RECT_SIZE;

    p.stroke(0, 0, 0, 20);
    p.rect(x, y, w, h);
    p.fill(0, 0, 0, 0);
  };
};

new p5(sketch);
