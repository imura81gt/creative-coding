import p5 from 'p5';
const FRAME_RATE = 3;
const MAX_RADIUS = 40;
const MIN_RADIUS = 20;
const sketch = (p: p5) => {
  p.setup = () => {
    const canvas = p.createCanvas(200, 200);
    p.frameRate(FRAME_RATE);
    canvas.parent('app1');
    p.pixelDensity(1);
  };
  p.draw = () => {
    let x = Math.random() * p.width;
    let y = Math.random() * p.height;
    let rad = Math.random() * (MAX_RADIUS - MIN_RADIUS) + MIN_RADIUS;

    p.noStroke();
    p.circle(x, y, rad);
    p.fill(0, 0, 0, 20);
  };
};

new p5(sketch);
