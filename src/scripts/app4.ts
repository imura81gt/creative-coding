import p5 from 'p5';
const FRAME_RATE = 60;
const MAX_RECT_SIZE = 100;
const MIN_RECT_SIZE = 35;
const sketch = (p: p5) => {
  p.setup = () => {
    const canvas = p.createCanvas(200, 200);
    canvas.parent('app4');
    p.frameRate(FRAME_RATE);
    p.pixelDensity(1);
    p.background(0);
  };
  p.draw = () => {
    let x = Math.random() * p.width;
    let y = Math.random() * p.height;
    let w = Math.random() * (MAX_RECT_SIZE - MIN_RECT_SIZE) + MIN_RECT_SIZE;
    let h = Math.random() * (MAX_RECT_SIZE - MIN_RECT_SIZE) + MIN_RECT_SIZE;

    p.noStroke();
    p.fill(255, 255, 255, 5);
    p.rect(x, y, w, h);

    p.textSize(12);
    p.textAlign(p.RIGHT);
    p.text('Good morning.', p.width - 2, p.height / 2);
    p.fill(255, 255, 255, 255);
  };
};

new p5(sketch);
