import p5 from 'p5';
const RADIUS = 30;
const sketch = (p: p5) => {
  p.setup = () => {
    p.createCanvas(200, 200);
  };
  p.draw = () => {
    if (p.mouseIsPressed) {
      p.fill(0);
    } else {
      p.fill(255);
    }

    for (var i = 0; i * RADIUS <= 200 + RADIUS; i++) {
      for (var j = 0; j * RADIUS <= 200 + RADIUS; j++) {
        if ((j + i) % 2 == 0) {
          p.arc(j * RADIUS, i * RADIUS, RADIUS, RADIUS, 0, p.PI);
        } else {
          p.arc(j * RADIUS, i * RADIUS, RADIUS, RADIUS, p.PI, 0);
        }
      }
    }
  };
};

new p5(sketch);
