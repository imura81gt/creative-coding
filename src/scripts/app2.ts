import p5 from 'p5';
const FRAME_RATE = 3;
const MAX_RADIUS = 100;
const MIN_RADIUS = 35;
const sketch = (p: p5) => {
  p.setup = () => {
    const canvas = p.createCanvas(200, 200);
    p.frameRate(FRAME_RATE);
    canvas.parent('app2');
    p.pixelDensity(1);
  };
  p.draw = () => {
    let x = Math.random() * p.width;
    let y = Math.random() * p.height;
    let rad = Math.random() * (MAX_RADIUS - MIN_RADIUS) + MIN_RADIUS;

  
    p.circle(x, y, rad);
    p.fill(0, 0, 0, 0);
  };
};

new p5(sketch);
