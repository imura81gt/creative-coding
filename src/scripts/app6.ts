import p5 from 'p5';
const TEXT_SIZE = 64;
const sketch = (p: p5) => {
  p.setup = () => {
    p.createCanvas(200, 200);
    p.textSize(TEXT_SIZE);
    p.textAlign(p.CENTER);
    p.fill(255);
  };
  p.draw = () => {
    p.background(0);
    p.text(p.key, 100, 100);
  };
};

new p5(sketch);
